#ifndef CASSYER_H
#define CASSYER_H

#include <QMainWindow>

namespace Ui {
class Cassyer;
}

class Cassyer : public QMainWindow
{
    Q_OBJECT

public:
    explicit Cassyer(QWidget *parent = 0);
    ~Cassyer();

private:
    Ui::Cassyer *ui;
};

#endif // CASSYER_H
