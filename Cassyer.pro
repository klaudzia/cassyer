#-------------------------------------------------
#
# Project created by QtCreator 2016-06-21T18:12:28
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Cassyer
TEMPLATE = app


SOURCES += main.cpp\
        cassyer.cpp

HEADERS  += cassyer.h

FORMS    += cassyer.ui
